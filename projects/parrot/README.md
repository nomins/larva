# Parrot - Slack Slash Command App

## Usage

채팅창 입력: `/parrot 내 말 따라하지마!`  

채팅창 응답: `내 말 따라하지마!`

## 참고

- [슬랙 명령어 API 공식 가이드](https://api.slack.com/slash-commands)
