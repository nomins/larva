'use strict';

/*
* express, bodyPaser를 사용 (튜토리얼에서 사용)
* init project
*/
const request = require('request');
const express = require('express');
const app = express();

// 환경 변수를 위한 dotenv 라이브러리
require('dotenv').config();

/**
 * SLACK 연결 하기위한 ID, SECRET, TOKEN
 */
var clientId = process.env.CLIENT_ID;
var clientSecret = process.env.CLIENT_SECRET;
var PORT = process.env.SERVER_PORT;

// 서버 시작
app.listen(PORT, function () {
    // port가 리슨시 아래 메시지 출력
    console.log("Example app listening on port " + PORT);
});


//  ngrok 주소 와 응답을 알려주는 Get Request 메시지
app.get('/', function(req, res) {
    res.send('Ngrok is working! Path Hit: ' + req.url);
});


// oauth를 호출
app.get('/oauth', function(req, res) {
    // When a user authorizes an app, a code query parameter is passed on the oAuth endpoint. If that code is not there, we respond with an error message
    if (!req.query.code) {
        res.status(500);
        res.send({"Error": "Looks like we're not getting code."});
        console.log("Looks like we're not getting code.");
    } else {

        // We'll do a GET call to Slack's `oauth.access` endpoint, passing our app's client ID, client secret, and the code we just got as query parameters.
        // We'll do a GET call to Slack's `oauth.access` endpoint, passing our app's client ID, client secret, and the code we just got as query parameters.
        request({
            url: 'https://slack.com/api/oauth.access', //URL to hit
            qs: {code: req.query.code, client_id: clientId, client_secret: clientSecret}, //Query string data
            method: 'GET', //Specify the method

        }, function (error, response, body) {
            if (error) {
                console.log(error);
            } else {
                res.json(body);

            }
        })
    }
});

// echotest 명령어로 endpoint 호출  
app.post('/echotest', function(req, res) {
    res.send('Your ngrok tunnel is up and running!');
});