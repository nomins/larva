'use strict';

/**
 * botkit 라이브러리를 사용
 * init project
*/
var Botkit = require('botkit');
require('dotenv').config();

// Slack 연결을 위한 변수를 체크
if (!process.env.CLIENT_ID || !process.env.CLIENT_SECRET || !process.env.VERIFICATION_TOKEN || !process.env.SERVER_PORT) {
    console.log(process.env.CLIENT_ID)
    console.log(process.env.CLIENT_SECRET)
    console.log(process.env.VERIFICATION_TOKEN)
    console.log(process.env.SERVER_PORT)
    console.log('Error: Specify CLIENT_ID, CLIENT_SECRET, VERIFICATION_TOKEN and SERVER_PORT in environment');
    process.exit(1);
}

/**
 *  Uses the slack button feature to offer a real time bot to multiple teams
 *  실시간 봇을 제공하는 slack button을 사용 
*/
var config = {}
if (process.env.MONGOLAB_URI) {
    var BotkitStorage = require('botkit-storage-mongo');
    config = {
        storage: BotkitStorage({mongoUri: process.env.MONGOLAB_URI}),
    };
} else {
    config = {
        json_file_store: './db_slackbutton_slash_command/',
    };
}

var controller = Botkit.slackbot(config).configureSlackApp(
    {
        clientId: process.env.CLIENT_ID,
        clientSecret: process.env.CLIENT_SECRET,
        scopes: ['commands'],
    }
);

/**
 *  webServer 확인
 */
controller.setupWebserver(process.env.SERVER_PORT, function (err, webserver) {
    controller.createWebhookEndpoints(controller.webserver);
    controller.createOauthEndpoints(controller.webserver, function (err, req, res) {
        if (err) {
            res.status(500).send('ERROR: ' + err);
        } else {
            res.send('Success!');
        }
    });
    console.log(controller.createOauthEndpoints)
});

/**
 * command 컨트롤러
 */

controller.on('slash_command', function (slashCommand, message) {

  switch (message.command) {
      case "/echotest": 
          // The rules are simple: If there is no text followin the command, view is as thouth it had requested "help" 
          // Botkit provides two functions for responding to a slash command: replyPublic() and replyPrivate().
          // replyPublic() will post the response into the Slack channel for everyone to see. 
          // replyPrivate() on the other hand will post an ephemeral message that only the user issuing the command can see.         

          // Tocken이 맞는치 확인
          if (message.token !== process.env.VERIFICATION_TOKEN) return; //just ignore it.

          // text가 아무것도 없가나 help로 들어올 경우 아래의 테스트 출력
          if (message.text === "" || message.text === "help") {
              slashCommand.replyPrivate(message,
                  "I echo back what you tell me. " +
                  "Try typing `/echotest hello` to see.");
              return;
          }

          // text가 있는 경우 입력된 메시지를 출력
          slashCommand.replyPublic(message, "1", function() {
              slashCommand.replyPublicDelayed(message, "2").then(slashCommand.replyPublicDelayed(message, "3"));
          });

          break;
      default:
          slashCommand.replyPublic(message, "I'm afraid I don't know how to " + message.command + " yet.");

  }력

})
;